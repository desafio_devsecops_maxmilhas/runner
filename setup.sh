#!/bin/bash

installAnsible() {
  apt-get update -y
  apt-get install software-properties-common -y
  add-apt-repository ppa:ansible/ansible-2.6 -y
  apt-get update -y
  apt-get install ansible -y
}

installGit() {
  apt-get update -y
  apt-get install git -y 
}

installDocker() {
  apt-get update -y
  apt-get install docker.io -y
}

installCurl() {
  apt-get update -y
  apt-get install curl -y
}

installCompose() {
  apt-get update -y
  curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose
}

#Verifica se o ansible esta instalado, caso nao esteja, faz a instalacao
if ! [ -x "$(command -v ansible)" ]; then
  installAnsible
fi

#verifica se o git esta instalado, caso nao esteja, faz a instalacao
if ! [ -x "$(command -v git)" ]; then
  installGit
fi

#instala o docker, caso nao esteja instalado
if ! [ -x "$(command -v docker)" ]; then
  installDocker
fi

#instala o curl, caso nao esteja instalado
if ! [ -x "$(command -v curl)" ]; then
  installCurl
fi

#instala o compose, caso nao esteja instalado
if ! [ -x "$(command -v docker-compose)" ]; then
  installCompose
fi

sysctl -w vm.max_map_count=262144

if [ ! -d "/opt/maxmilhasec/" ]; then
     mkdir -p /opt/maxmilhasec
fi

if [ ! -d "/opt/maxmilhasec/volumes/servers/db/server1" ]; then
     mkdir -p /opt/maxmilhasec/volumes/servers/db/server1
fi

if [ ! -d "/opt/maxmilhasec/volumes/servers/db/server2" ]; then
     mkdir -p /opt/maxmilhasec/volumes/servers/db/server2
fi

if [ ! -d "/opt/maxmilhasec/volumes/servers/db/server3" ]; then
     mkdir -p /opt/maxmilhasec/volumes/servers/db/server3
fi

cd /opt/maxmilhasec

if [ ! -d "/opt/maxmilhasec/images" ]; then
   git clone https://gitlab.com/desafio_devsecops_maxmilhas/images.git
fi

cd images
docker-compose build --no-cache --force-rm 
docker-compose up --force-recreate